import numpy as np
import os
import string
import time
import math
import random
import sys
import re
import warnings
import pandas as pd
import h5py
import os
import sys
import pdb, shutil
from IPython.display import display, Image, SVG
from ipywidgets import widgets, interact, Checkbox, Text, VBox, HBox, Dropdown, interactive, IntSlider, Button
import aux_functions as af
import load_flavors_func as lff
# sys.path.append(os.path.abspath(os.path.join(os.curdir)))
# sys.path.append(os.path.abspath('.'))
# sys.path.append(os.path.abspath(os.path.join(os.curdir, '..', 'pycleaner', 'functions')))
# from matplotlib.ticker import FormatStrFormatter
# from ipywidgets import widgets, Text, interactive, interact
# from IPython.display import display


def ini_settings():
    # testDates = os.listdir(r'C:\PhD_Emilio\Emilio_PhD\Field_Data\_RawData\Pumping\HydraulicData')#['02062016', '10062016', '22062016', '24062016', '01072016']
    # mydate = widgets.Dropdown(description='TestDate:', options=testDates)
    checkboxes = []
    cb_container = widgets.HBox(align_items='stretch', border='3px solid black', display='flex' )
    display(cb_container)

    renamefiles = checkboxes.append(widgets.Checkbox(description='RenameFiles', value=False, width=80))
    joindata = checkboxes.append(widgets.Checkbox(description='JoinDatasets', value=True,width=80))
    ToPyCleaner = checkboxes.append(widgets.Checkbox(description='ToPyCleaner', value=False,width=80))
    ToDatabase = checkboxes.append(widgets.Checkbox(description='ToDataBase', value=False, width=80))
    plotlyAcc = checkboxes.append(widgets.Checkbox(description='Plotly', value=True, width=80))
    ViewPlots = checkboxes.append(widgets.Checkbox(description='ViewPlots', value=False, width=80))
    # injection_time = widgets.Checkbox(description='Tracer injection time (HH:MM:SS)', value=False)
    # display(mydate, renamefiles, joindata, plotlyAcc, ToPyCleaner, ToDatabase)
    # display(HBox([renamefiles, joindata, plotlyAcc, ToPyCleaner, ToDatabase]))
    # display(renamefiles, joindata, plotlyAcc, ToPyCleaner, ToDatabase)
    # return mydate, renamefiles, joindata, plotlyAcc, ToPyCleaner, ToDatabase
    cb_container.children = [i for i in checkboxes]
    return cb_container.children
    # return renamefiles, joindata#, plotlyAcc, ToPyCleaner, ToDatabase, ViewPlots


def update_button(mydatabase):
    """ Function to create a button to update back up database..."""
    def upd_backup(b):
        shutil.copyfile(mydatabase.value, '%s_bckup.hdf5' % mydatabase.value[:-5])
        print('Backup Database successfully updated!')

    mydescr = 'Click HERE to update the backup database'
    button = Button(description=mydescr, background_color='#AEC', remove=True)
    display(button)
    button.on_click(upd_backup)


def loadButton(database, sel_screen, screen_id_file, How2load, what2load, mydatabase, ans1, sample_collection_menu, responsible, appendset):
    """ Widget that generates a button to finally load the datasets to the DataBase
    """
    def on_myclick_D(b):
        lff.loadmanual(what2load, database, sel_sourcedir, screen_id_file, sel_date, sel_well, selected_screen, responsible,
                       samptype=sel_samptype, datatype=sel_datatype, sampmethod=sel_samplemethod, appendsets=appendset)

    def on_myclick(b):
        lff.loadbatch(what2load, database, ans1.value, screen_id_file, appendsets=appendset)

    mydescr = 'CLICK HERE to make a < %s > load of < %s > to database < %s >' % (How2load, what2load, mydatabase)
    button = Button(description=mydescr, background_color='#CAC', remove=True)
    display(button)

    if (How2load == 'manual') or (How2load == 'single'):
        if what2load == 'wells':
            sel_sourcedir = ans1.value
            sel_date = None
            sel_samptype = None
            sel_datatype = None
            sel_samplemethod = None
            selected_screen = None
            sel_well = None
        elif what2load == 'datasets':
            sel_sourcedir = sample_collection_menu.result[4]
            sel_date = sample_collection_menu.result[0]
            sel_samptype = sample_collection_menu.result[1]
            sel_datatype = sample_collection_menu.result[2]
            sel_samplemethod = sample_collection_menu.result[3]
            selected_screen = sel_screen.value
            try:
                sel_well = ans1.value
            except AttributeError:
                sel_well = ans1.result

        button.on_click(on_myclick_D)
        database.flush()

    if How2load == 'batch':
        button.on_click(on_myclick)


def createwidget(mydescription, widgettype='text', myoptions=[], bc='#AEE', defaultvalue=''):
    """ Creates either a text or a dropdown widget in jupyter-notebook
        Args:
    mydescription:      str, title of the Widget
    widgettype:         str, type of widget. 'text' or 'dropdown'
    myoptions:          list, just for 'dropdown', containing the possible choices
    bc:                 str, background color code for the widget
    defaultvalue:       str, value to show as default in the widget
        Returns:
    Widget object
    """
    if widgettype == 'text':
        mywidget = Text(description=mydescription, width='100%', background_color=bc, value=defaultvalue)

    elif widgettype == 'dropdown':
        mywidget = Dropdown(description=mydescription, options=myoptions, background_color=bc, value=defaultvalue)

    display(mywidget)

    return mywidget


def existingwells(WellName, database):
    """ Creates a Dropdown widget showing the available wells and their existing well screen IDs
        Arguments:
    WellName:       list, well IDs within the database
        Returns:
    WellName and screen_ID  selected from the dropdown menu
    """
    if WellName in list(database.keys()):
        screen_list = list(database['%s/Location' % WellName].keys())
        screens = []
        for aa in screen_list:
            if 'Screen' in aa:
                screens.append(aa)
        print('Well %s contains the following screen ID\'s: %s' % (WellName, screens))
        screen_ID = createwidget('Screen ', widgettype='dropdown', myoptions=screens, bc='#AAB')
        display(screen_ID)
    if WellName not in list(database.keys()):
        print('Well %s not in database' % WellName)
    return WellName, screen_ID


def how2load(what2load, toprint=''):
    """ Calls the createwidget function to generate corresponding menus for 'wells' or 'datasets'
        Args:
    what2load:      str, 'wells' or 'datasets'
    toprint:        str, banner to print properly the available wells
        Returns:
    dropdown widget object
    """
    if what2load == 'wells':
        print('Existing wells: ', toprint)
        How2load = createwidget('How2load ', widgettype='dropdown', myoptions=['batch', 'single'],
                                bc='#AAB', defaultvalue='single')

    elif what2load == 'datasets':
        How2load = createwidget('How2load ', widgettype='dropdown', myoptions=['batch', 'manual'],
                                bc='#AAB', defaultvalue='manual')

    return How2load


def sourcedirWidget(what2load, How2load, welllist=''):
    """ Creates a Widget to define the source directory where the information needed is to be found

        Args:
    what2load:      str, what to load, 'wells', 'datasets'
    How2load:       str, how to load,   'single', 'manual', 'batch'
    welllist:       list, wells available, used only if manual or single load of datasets
        Returns:
    either the well name box widget or the source directory text widget
    """
    if (How2load == 'single') or (How2load == 'manual'):

        if what2load == 'wells':
            default_path = r'...\B10_Well_v2.xlsx'
            sourcedir = Text(description='SourceDir ', width='100%', background_color='#AEE', value=default_path)
            display(sourcedir)
            sourcedir.on_displayed(print('Type in full directory of excel control file, including filename'))
            sourcedir.on_submit(af.def_dir)

        elif what2load == 'datasets':
            try:
                def callwells(ListWells):
                    return ListWells

                well_name = interactive(callwells, ListWells=welllist['Wells'])
                display(well_name)
            except ValueError:
                print('Be sure to have the wells of interest already in the database!')


    elif How2load == 'batch':

        if what2load == 'wells':
            default_path = r'...\LauswiesenDatabase\Well_Files'
            sourcedir = Text(description='SourceDir ', width='100%', background_color='#AEE', value=default_path)
            display(sourcedir)
            sourcedir.on_displayed(print('Type the directory containing the WELL files of interest'))
            sourcedir.on_submit(af.def_dir)

        elif what2load == 'datasets':
            default_path = r'...\LauswiesenDatabase\ipynb_2_DataSetsUpload'
            sourcedir = Text(description='SourceDir ', width='100%', background_color='#AEE', value=default_path)
            display(sourcedir)
            sourcedir.on_displayed(print('Be sure to have the correct file name convention and directory structure!'))
            sourcedir.on_submit(af.def_dir)

    if (what2load == 'datasets') and ((How2load == 'single') or (How2load == 'manual')):
        return well_name
    else:
        return sourcedir


def samCollection(database, wellname, what2load, How2load):
    """ Creates proper widgets to fill in Sample Collection information. Just for manual load of datasets.
        Args:
    database:       hdf5 database
    wellname:       str, well ID
    what2load:      str, what to load, 'wells', 'datasets'
    How2load:       str, how to load,   'single', 'manual', 'batch'
        Returns:
    sample_collection_menu and sel_screen
    """
    if (what2load == 'datasets') and (How2load == 'manual'):
        # testinfo_widgets = dirWidget(mymenus=['FlowRate', 'Wells', 'FlowUnits', 'TracerMass', 'MassUnits', 'MassWell', 'TypeTest',
        #                    'TestSetup', 'Remarks'], mytitle='TestInfo', defaults=['MyNameNoSpaces',
        #                     '4.89,2.25,0.822,-2', 'B2,B3TOP,B3MID,B6', 'literspersecond', '2.0', 'grams', 'B3BOT',
        #                     'forcedgradient','nested','tracerTomography'])
        # testinfo_values = []
        # [testinfo_values.append(ii.value) for ii in testinfo_widgets]
        # for ii in testinfo_widgets:
        #     testinfo_values.append(ii.value)
        # responsible = createwidget('Responsible', widgettype='text', bc='#AEE', defaultvalue='MyNameNoSpaces')
        # flowrate = createwidget('FlowRate', widgettype='text', bc='#AEE', defaultvalue='4.89,2.25,0.822,-2')
        # wells =  createwidget('Wells', widgettype='text', bc='#AEE', defaultvalue='B2,B3TOP,B3MID,B6')
        # flowunits = createwidget('FlowUnits', widgettype='text', bc='#AEE', defaultvalue='literspersecond')
        # tracermass = createwidget('TracerMass', widgettype='text', bc='#AEE', defaultvalue='2.0')
        # massunit = createwidget('MassUnit', widgettype='text', bc='#AEE', defaultvalue='grams')
        # masswell = createwidget('MassWell', widgettype='text', bc='#AEE', defaultvalue='B3BOT')
        # typetest = createwidget('TypeTest', widgettype='text', bc='#AEE', defaultvalue='forcedgradient')
        # testsetup = createwidget('TestSetup', widgettype='text', bc='#AEE', defaultvalue='nested')
        # remarks = createwidget('Remarks', widgettype='text', bc='#AEE', defaultvalue='tracerTomography')
        # testinfo_widgets = [responsible.value, flowrate.value, wells.value, flowunits.value, tracermass.value, massunit.value,
        #                     masswell.value, typetest.value, testsetup.value, remarks.value]
        myscreens = af.existingscreens(wellname, database)
        sel_screen = createwidget('Screens', widgettype='dropdown', myoptions=myscreens, bc='#AEE',
                                     defaultvalue=myscreens[0])
        sample_collections_lookup = {
            'SampleType': ['pumpingtest', 'tracertest', 'monitoring', 'laboratory'],
            'SampleMethod': ['evolution', 'graphtec', 'manual water level'],
            'SampleMedium': ['groundwater', 'soil', 'surface water', 'air'],
            'parameterType': ['field', 'calculated', 'laboratory']
        }

        def samplecollection_fun(SampleDate, SampleType, DataType, SampleMethod, FileSource):
            return SampleDate, SampleType, DataType, SampleMethod, FileSource

        sample_collection_menu = interactive(samplecollection_fun,
                                             SampleDate=Text(width=200, background_color='#AEE', value='24-12-16'),
                                             SampleType=sample_collections_lookup['SampleType'],
                                             DataType=sample_collections_lookup['parameterType'],
                                             SampleMethod=sample_collections_lookup['SampleMethod'],
                                             FileSource=Text(width=200, background_color='#AEE',
                                                             value='C:\SomePath\Somefile.txt')
                                             )
        display(sample_collection_menu)

        return sample_collection_menu, sel_screen  #, testinfo_values

    else:
        print('Nothing to process here, continue with the next section')
        return None, None


def dirWidget(mymenus=[], mytitle='', defaults=[]):
    if len(mymenus) > 0:
        mypaths = []
        for idx, cur_menu in enumerate(mymenus):
            path = widgets.Text(description=cur_menu, width='100%', padding=4, value=defaults[idx])
            mypaths.append(path)

    menu = widgets.Box(children=mypaths, width='100%')
    accord = widgets.Accordion(children=[menu], width='100%')
    display(accord)
    accord.set_title(0, mytitle)
    return mypaths
