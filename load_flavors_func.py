import os
import sys
import h5py
import aux_functions as af
import pandas as pd
import numpy as np
import math
import pdb
# import sys
sys.path.append(os.path.abspath(os.path.join(os.curdir, '..', 'pycleaner', 'functions')))
# sys.path.append(os.path.abspath(os.path.join(os.curdir, '..', 'pycleaner', 'functions')))
import fielddata as fd


def loadmanual(process, database, sourcedir, screen_id_file, date, well, screen, responsible, samptype='pumpingtest', datatype='field', sampmethod='evolution', sampmethod_metadata='', appendsets=False):
    """ Make a manual load of Wells or datasets to the database
        Args:
    process:
    database:
    sourcedir:
    screen_id_file:
    date:
    well:
    screen:
    samptype:
    datatype:
    sampmethod:
        Returns:

    """
    if type(database) == str: # If a string instead of a h5py object is passed
        database = h5py.File(database, 'a')

    if process == 'wells':
        loadfromexcel(sourcedir, database, screen_id_file)

    elif process == 'datasets':
        # First ensure the subgroup tree exists:
        date_group = af.addgroup(database['/%s/Location/%s' % (well, screen)], groupname=date)
        samplingtype_group = af.addgroup(date_group, groupname=samptype)
        paramtype_group = af.addgroup(samplingtype_group, groupname=datatype)
        samplemethod_group = af.addgroup(paramtype_group, groupname=sampmethod)
        for cur_key in responsible:
            samplemethod_group.attrs[cur_key] = responsible[cur_key]

        for ii in range(0, len(sampmethod_metadata.index)):
            samplemethod_group.attrs[sampmethod_metadata['field'][ii]] = sampmethod_metadata['value'][ii]

        # samplemethod_group = database['/%s/Location/%s/%s/%s/%s/%s' % (well, screen, date, samptype, datatype, sampmethod)]
        loadsets(sourcedir, sampmethod, samplemethod_group, appendset=appendsets)

    database.flush()


def loadbatch(process, database, sourcedir, screen_id_file, appendsets=False):
    """ Make a batch load of either Wells or datasets
        Args:
    process:
    database:
    sourcedir:
    screen_id_file:
        Returns:

    """
    if type(database) == str: # If a string instead of a h5py object is passed
        database = h5py.File(database, 'a')

    if process == 'wells':  # To load wells, an excel file is mandatory
        excelfiles = os.listdir(sourcedir)

        for cur_file in excelfiles:
            if '~$' not in cur_file:
                loadfromexcel(os.path.join(sourcedir, cur_file), database, screen_id_file)
                print('Batch-load in process: Information from file < %s > loaded into database' % (os.path.join(sourcedir, cur_file)))

    elif process == 'datasets':  # To load datasets in batch, the proper folder tree structure is mandatory
        mydates = os.listdir(sourcedir)

        for mydate in mydates:
            if '.png' in mydate:
                pass
            else:
                samplingtype_list = os.listdir(os.path.join(sourcedir, mydate))  # e.g. pumping test

                for cur_samplingtype in samplingtype_list:
                    paramtype_list = os.listdir(os.path.join(sourcedir, mydate, cur_samplingtype))  # eg. field

                    for cur_paramtype in paramtype_list:
                        samplemethod_list = os.listdir(
                            os.path.join(sourcedir, mydate, cur_samplingtype, cur_paramtype))  # eg. fiber optic

                        for cur_samplemethod in samplemethod_list:
                            num_measurements_list = os.listdir(
                                os.path.join(sourcedir, mydate, cur_samplingtype, cur_paramtype, cur_samplemethod))
                            num_measurements_list = [x for x in num_measurements_list if ('setup' not in x) and ('_config' not in x)]
                            try:
                                responsibleFile = 'testInfo.txt'
                                responsible = num_measurements_list[num_measurements_list.index(responsibleFile)]
                                num_measurements_list.remove(responsibleFile)
                                responsible = pd.read_table(os.path.join(sourcedir, mydate, cur_samplingtype,
                                                            cur_paramtype, cur_samplemethod, 'testInfo.txt'),
                                                            header=None, names=['field', 'value'])
                                resp = True
                            except:
                                resp = False
                                print('Errors in testInfo.txt or file not found, this will not be added as metadata')

                            for cur_idx, cur_nummeas in enumerate(num_measurements_list):
                                myfile_list = os.listdir(
                                    os.path.join(sourcedir, mydate, cur_samplingtype, cur_paramtype, cur_samplemethod,
                                                 cur_nummeas))

                                # Get well ID and well screen ID from file names:
                                for cur_file in myfile_list:
                                    if 'calib' not in cur_file:  # For the laboratory
                                        bibliography = os.path.join(sourcedir, mydate, cur_samplingtype, cur_paramtype,
                                                                    cur_samplemethod, cur_nummeas, cur_file)

                                        well_ID = (cur_file.split('.')[0]).upper()

                                        if ('cmt' in well_ID) or ('CMT' in well_ID):
                                            screen_ID = well_ID[-1]
                                            well_ID = well_ID[:-1]
                                        elif ('BOT' in well_ID) or ('MID' in well_ID) or ('TOP' in well_ID):
                                            screen_ID = well_ID[-3:]
                                            well_ID = well_ID[:-3]
                                        # elif '_' in well_ID:
                                        #     screen_ID = well_ID.split('_')[-1]
                                        else:
                                            screen_ID = well_ID
                                        try:
                                            screen_group, screen_uniqueid, existScreen = af.addgroup(
                                                database['%s/Location' % well_ID], groupname='Screen_%s' % screen_ID,
                                                idfile=screen_id_file, wellname=well_ID)
                                            date_group = af.addgroup(screen_group, groupname=mydate)
                                            samplingtype_group = af.addgroup(date_group, groupname=cur_samplingtype)
                                            paramtype_group = af.addgroup(samplingtype_group, groupname=cur_paramtype)
                                            samplemethod_group = af.addgroup(paramtype_group, groupname=cur_samplemethod)
                                            if resp is True:
                                                for ii in range(0, len(responsible.index)):
                                                    if responsible['field'][ii] not in samplemethod_group.attrs:
                                                        samplemethod_group.attrs[responsible['field'][ii]] = responsible['value'][ii]
                                        except KeyError:
                                            sys.exit(
                                                'Well not found. First generate a record (excel) for well %s' % well_ID)

                                        loadsets(bibliography, cur_samplemethod, samplemethod_group, appendset=appendsets)
                                        database.flush()

    print('Finished loading data')


def loadfromexcel(myfiledir, database, screen_id_file):
    """ Prepares to Load datasets to an H5py database using all information
    provided in excel control file.
        Arguments:
    myfiledir:       str,  directory (including filename) of file to load
    database:        h5object or string, database of interest
    screen_id_file:  str, full path (inc. filename) of the screen id associated to each dataset
        Return:
    Updated (or not) database
    """
    if type(database) == str: # If a string instead of a h5py object is passed
        database = h5py.File(database, 'a')

    # Load all data from excel, this should be done just in case a new well is added:
    locationdata = pd.read_excel(os.path.join(myfiledir), sheetname='Location',
                                 header=0, skiprows=1, parse_cols="A,B")
    datumdata = pd.read_excel(os.path.join(myfiledir), sheetname='Datum',
                              header=0, skiprows=1, parse_cols="A,B")
    depthdata = pd.read_excel(os.path.join(myfiledir), sheetname='WellDepth',
                              header=0, skiprows=1, parse_cols="A,B")
    lithologydata = pd.read_excel(os.path.join(myfiledir), sheetname='Lithology',
                                  header=0, skiprows=1, parse_cols="A,B")
    screendata = pd.read_excel(os.path.join(myfiledir), sheetname='WellScreen',
                               header=0, skiprows=1, parse_cols="A,B")

    data_dict = {'a': locationdata, 'b': datumdata, 'c': depthdata, 'd': lithologydata, 'e': screendata}

    for cur_key, cur_value in data_dict.items():
        cur_value.columns = ['field', 'entry']

    well_name = locationdata.entry[0]
    screen_name = screendata.entry[np.where(screendata.field == 'ScreenName')[0][0]]

    # Drop extra entries in Lithology and well depth
    depthdata = depthdata.dropna(subset=['entry']).reset_index(drop=True)
    lithologydata = lithologydata.dropna(subset=['entry']).reset_index(drop=True)
    datumdata = datumdata.dropna(subset=['entry']).reset_index(drop=True)
    # Main Container and ID for the well of interest:
    try:
        print(well_name)
        mainContainer = database.create_group(well_name)
        id_mode = 'newwell'
        print('New well added to the database: %s' % locationdata.entry[0])
    except ValueError:
        # This will read the group if already exists
        print('Working on existing well: %s ' % well_name)  # Todo: set up a pause so the user can decide
        mainContainer = database.require_group(locationdata.entry[0])
        id_mode = 'existwell'

    # Create Well sub groups if needed, if not then just get the hdf5 objects to work with them:
    locationGroup = af.addgroup(mainContainer, groupname='Location')
    datumGroup = af.addgroup(mainContainer, groupname='Datum')
    depthGroup = af.addgroup(locationGroup, groupname='WellDepth')
    lithologyGroup = af.addgroup(locationGroup, groupname='Lithology')
    screenGroup, cur_screen_id, existScreen= af.addgroup(locationGroup, groupname='Screen_%s' % screen_name, idfile=screen_id_file, wellname=well_name)

    # Add bibliographic source:
    screenGroup.attrs['BibliographicSource'] = myfiledir

    # Add attributes from Excel file
    data_dict = {'a': locationdata, 'b': datumdata, 'c': depthdata, 'd': lithologydata, 'e': screendata}
    group_dict = {'a': locationGroup, 'b': datumGroup, 'c': depthGroup, 'd': lithologyGroup, 'e': screenGroup}

    # If new well, add metadata (from excel file) to each subgroup:
    if (id_mode == 'existwell') and (existScreen is True):
        print('Screen well < %s > also exists, nothing has been loaded to the database' % screenGroup.name)
        pass
    elif (id_mode == 'existwell') and (existScreen is False):
        print('Loading new screen well < %s > to well < %s >' % (cur_screen_id, locationdata.entry[0]))
        af.addattrs(group_dict['e'], data_dict['e'])
    elif id_mode == 'newwell':
        for cur_keys in data_dict:
            af.addattrs(group_dict[cur_keys], data_dict[cur_keys])
    database.flush()
    # database.close()

    return


def loadsets(myfiledir, sampleMethod, samplemethod_group, appendset=False):
    """ Function that actually loads the dataset into the hdf5 database based on a source file
        Arguments:
    myfiledir:      str, full directory(incl filename) containing the data to be loaded
    sampleMethod:   str, describes the sample method used to collect the data:
                    'evolutionfiberopt_presstrans', 'graphtectrans', 'hermesfiberopt_fluortrans'
    samplemethod_group:    hdf5 group object, sample group (e.g. measParamType:field, processed...) where
                            data is to be stored
    full_output:    bool, include metadata in returned variables or not
        Return:
    loaded dataset, if full_outup is True, also the metadata loaded from each source file.
    """
    readFlag = True
    if sampleMethod == 'evolution':
        meas_label = 'WaterColumn_1'
        datetime_label = 'DateTime_1'

    elif (sampleMethod == 'graphtec') or (sampleMethod == 'hermes') or (sampleMethod == 'fluoromax'):
        meas_label = 'Measurement_1'
        datetime_label = 'DateTime_1'

    if ('%s/%s' % (samplemethod_group.name, meas_label) in samplemethod_group) and (appendset is True):
        # Add 1 to the meas_label name if a dataset already exists, to create an additional dataset:
        dataset_list = list(samplemethod_group.keys())
        dataset_list.sort()
        basket = int(dataset_list[-1].split('_')[-1])
        basket += 1
        meas_label = '%s_%s' % (meas_label.split('_')[0], basket)
        datetime_label = '%s_%s' % (datetime_label.split('_')[0], basket)

    elif ('%s/%s' % (samplemethod_group.name, meas_label) in samplemethod_group) and (appendset is False):
        print('Conflict Dataset %s/%s. Skip data...' % (samplemethod_group.name, meas_label))
        readFlag = False
    mypath = os.path.split(myfiledir)[0]
    cur_file = os.path.split(myfiledir)[-1]
    if readFlag is True:
        if 'field' in samplemethod_group.name:
            if sampleMethod == 'evolution':
                mydataset, metadata = fd.load_fiberoptic(mypath, cur_file, getMetadata=True)
                mydatetime = (mydataset.values[:, 0] + ' ' + mydataset.values[:, 1]).astype('S')
                mydataset.drop([mydataset.columns[0], mydataset.columns[1]], axis=1, inplace=True)

            elif sampleMethod == 'graphtec':
                myfullset, metadata = fd.load_graphtec_single(os.path.join(mypath, cur_file), skipr=8, get_metadata=True)
                mydataset = myfullset['value']
                mydatetime = myfullset['date']
                # del mydataset['date']

            elif sampleMethod == 'hermes':
                mydataset, metadata = fd.load_tracer19_single(os.path.join(mypath, cur_file), skipr=16, get_metadata=True)
                mydatetime = mydataset['date']
                del mydataset['date']

            if '%s/%s' % (samplemethod_group.name, meas_label) not in samplemethod_group:
                samplemethod_group.create_dataset(datetime_label, data=mydatetime.astype('|S'), compression="gzip")
                samplemethod_group.create_dataset(meas_label, data=mydataset, dtype=np.float32, compression="gzip")
                af.addattrs_dict(samplemethod_group[meas_label], metadata)
                # samplemethod_group[meas_label].attrs['columnLabels'] = columlabel
                print('Data has been loaded successfully at:\n>>  %s' % samplemethod_group[meas_label].name)

        elif 'laboratory' in samplemethod_group.name:

            if sampleMethod == 'fluoromax':     # Search if there is a calib_curve file:
                try:
                    calib_file = [x for x in os.listdir(mypath) if 'calib' in x]
                    mydataset, metadata = fd.load_fluoromax_excel(os.path.join(mypath, calib_file[0]), skipr=2, get_metadata=True)
                    samplemethod_group.create_dataset('CalibrationCurve', data=mydataset, compression="gzip")
                except:
                    print('No file with a calibration curve has been found')
                    pass

                if 'calib' not in myfiledir:
                    myfullset, metadata = fd.load_fluoromax_excel(myfiledir, skipr=5, get_metadata=True)
                    mydataset = myfullset['cps']
                    mydatetime = myfullset['time']

                if '%s/%s' % (samplemethod_group.name, meas_label) not in samplemethod_group:
                    samplemethod_group.create_dataset(datetime_label, data=mydatetime.astype('|S'), compression="gzip")
                    samplemethod_group.create_dataset(meas_label, data=mydataset, dtype=np.float32, compression="gzip")
                    af.addattrs_dict(samplemethod_group[meas_label], metadata)
                    # samplemethod_group[meas_label].attrs['columnLabels'] = columlabel
                    print('Data has been loaded successfully at:\n>>  %s' % samplemethod_group[meas_label].name)

        else:
            print('SampleType and/or SampleMethod name does not follow the available options')
            pass
