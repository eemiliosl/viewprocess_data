import numpy as np
import pandas as pd
import h5py
import os, sys
import load_flavors_func as ldf
import aux_functions as auxf
import math
import tables
import matplotlib.pylab as plt
sys.path.append(os.path.abspath(os.path.join(os.curdir, '..', 'pycleaner','functions')))
import pycleaner.functions.fielddata as fd
import pycleaner.functions.miscel as ms

# Initial Settings
my_path = r'C:\Users\IRTG\Dropbox\LauswiesenDatabase\_Processed_Data\tracertest\hermes\17072015\17072015_single'

renameFiles = False
collectDataset = True
plotallbtc = False
Moments = True
dates = os.listdir(my_path)
common_str = 'conc'
pumpRate = 2.4  # L/s
InjMass = 2110# mg

myfiles, dummy = ms.getdirs(my_path, mystr='conc', fullpath=True)
# myfiles = [x for x in myfiles if not 'B6TOP' in x]  # Discard specified btc´s
newList = []
for bb in myfiles: newList.append(bb.split('\\')[-1][:-15])
momentID = ['Zeroth', 'First', 'Second', 'FirstN', 'SecondN', 'SecondC', 'SecondCN', 'ThirdCN', 'Mass', 'Recovery'] # 'MeanVel'

df = pd.DataFrame(index=momentID, columns=newList)
# Plot all BTC together:
for ii, cur_file in enumerate(myfiles):

    cur_data = np.loadtxt(cur_file)
    dataObj = ms.Miscel(cur_data[:, 0], cur_data[:, 1], mytitles=cur_file.split('\\')[-3], plotting=False)

    # Moments
    if Moments is True:
        Zero_m = dataObj.tempmoments(degree=0, central=False, full_output=False)
        First_m = dataObj.tempmoments(degree=1, central=False, full_output=False)
        Second_m = dataObj.tempmoments(degree=2, central=False, full_output=False)
        First_mN = First_m / Zero_m
        Second_mN = Second_m / Zero_m
        Second_mC = Second_m - (First_m**2 / Zero_m)
        Second_mCN = Second_mC / Zero_m
        Third_mCN = dataObj.tempmoments(degree=3, central=True, full_output=False) / Zero_m
        Mass = Zero_m * pumpRate
        Recovery = Mass / InjMass
        # MeanVelocity = d[ii] / First_mN
        allValues = [Zero_m, First_m, Second_m, First_mN, Second_mN, Second_mC, Second_mCN, Third_mCN, Mass, Recovery]
        for zz, curMoment in enumerate(momentID):
            df[cur_file.split('\\')[-1][:-15]][curMoment] = allValues[zz]# [0]
        cumsum_emp = dataObj.cumsum(normalized=False)
    if plotallbtc is True:
        plt.figure(1)
        plt.plot(cur_data[:, 0], cur_data[:, 1], '-', label=os.path.split(cur_file)[-1][:-15])
        plt.figure(2)
        plt.plot(cumsum_emp[0], cumsum_emp[1], '-', label=os.path.split(cur_file)[-1][:-15])
        # print('debugging...')

df.to_csv(os.path.join(my_path, '..', '%s_moments.txt' % my_path.split('\\')[-2]), sep='\t')

if plotallbtc is True:
    plt.figure(1)
    plt.grid(True)
    plt.legend()
    plt.title(os.path.split(os.path.split(dummy)[0])[-1])
    plt.xlabel('Time[seconds]')
    plt.ylabel('Concentration[mg/L]')

    plt.figure(2)
    plt.grid(True)
    plt.legend()
    plt.title(os.path.split(os.path.split(dummy)[0])[-1])
    plt.xlabel('Time[seconds]')
    plt.ylabel('Concentration[mg/L]')
    plt.show()



# Get the final dataset for inversion:
if collectDataset is True:
    # for ii in dates:
    #     datasetFull, wells, times = fd.collectFielddata(os.path.join(my_path,ii),
    #                                              outputfile=os.path.join(my_path, ii, 'Field%sfl_.dat' % ii),
    #                                              id_str='flip', wellnamesFile=True, timesFile=True, del_1stpoint=True)

    datasetFull, wells, times = fd.collectFielddata(os.path.join(my_path, '01072016_single'),
                                                    outputfile=os.path.join(my_path, '01072016_single', 'Field%str_.dat' % '01072016'),
                                                    id_str=common_str, wellnamesFile=True, timesFile=True, del_1stpoint=False)

if renameFiles is True:
    auxf.correctFileNames(my_path)

dates = os.listdir(my_path)
test_strings = ['4-4']
# if cmt in string strip: test_strings[0].replace('-','')
# if not cmt, and not modif....
# if not cmt and modif....




cur_dates = ['07072015', '22072015']#['26062015', '17072015', '02062016']

filteredFiles1 = os.listdir(os.path.join(my_path, cur_dates[0]))
filteredFiles1 = [x for x in filteredFiles1 if (common_str in x and '4-4' in x) or (common_str in x and 'CMT41' in x) or (common_str in x and 'CMT33' in x)]

filteredFiles2 = os.listdir(os.path.join(my_path, cur_dates[1]))
filteredFiles2 = [x for x in filteredFiles2 if (common_str in x and 'CMT41' in x) or (common_str in x and '4-4' in x) or (common_str in x and 'CMT33' in x)]

# filteredFiles3 = os.listdir(os.path.join(my_path, cur_dates[1]))
# filteredFiles3 = [x for x in filteredFiles3 if (common_str in x and 'CMT41' in x) or (common_str in x and '4-4' in x)]

# Load the data:

for xx in range(0, len(filteredFiles1)):
    cur_set1 = pd.read_table(os.path.join(my_path, cur_dates[0], filteredFiles1[xx]), names=['time', 'heads'], sep=' ')
    plt.plot(cur_set1.time, cur_set1.heads, 'bx-', label=cur_dates[0])
    cur_set2 = pd.read_table(os.path.join(my_path, cur_dates[1], filteredFiles2[xx]), names=['time', 'heads'], sep=' ')
    plt.plot(cur_set2.time, cur_set2.heads, 'r^-', label=cur_dates[1])
    plt.legend(loc='best')
    plt.grid(True, which='minor')
    # cur_set3 = pd.read_table(os.path.join(my_path, cur_dates[2], filteredFiles3[xx]), names=['time', 'heads'], sep=' ')
    # plt.plot(cur_set3.time, cur_set3.heads, 'ko-')
    plt.show()

print('pause')

