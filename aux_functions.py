from matplotlib.ticker import FormatStrFormatter
from IPython.display import display, Image, SVG
from ipywidgets import widgets, interact, Checkbox, Text, VBox, HBox, Dropdown, interactive, IntSlider, Button
import os
import sys
import pdb
import numpy as np
import string
import time
import math
import random
import re
import warnings
import pandas as pd
import h5py


def dbstructure(name):
    """ Scan through Database Structure
        Arguments:
    name:   str, current node of the database
    """
    print(name)
    mylist.append(name)
    #  return mylist


def def_dir(sender):
    mypath = r'%s' % sender.value
    if (os.path.isdir(mypath)is False) and (os.path.isfile(mypath) is False) :
        print('Directory < %s > is NOT valid!!!' % mypath)
    elif os.path.isdir(mypath):
        print('Directory < %s > is valid' % mypath)
        return mypath


def create_dir(dirs=''):
    if len(dirs) > 0:
        # pdb.set_trace()
        for cur_dir in dirs:
            if os.path.isdir(cur_dir):
                print('Directory < %s > already exists' % cur_dir)
            if not os.path.isdir(cur_dir):
                try:
                    os.makedirs(cur_dir)
                    print('Directory < %s > created' % cur_dir)
                except FileNotFoundError:
                    print('Directory < %s > cannot be created' % cur_dir)


def addgroup(mycontainer, groupname='default', idfile='', wellname=''):
    """ Add a group to a given hdf5 container, with the provided name. If dealing with well screen, a uique ID is
    either retrieved or generated.
        Arguments:
    mycontainer:    hdf5 group object, group that will contain the specified subgroup
    groupname:      str, name to assign to the group
    idfile:         str, path of the unique ids. Needed only if dealing with Well screens
    wellname:       str, well name. Only if dealing with Well screens
        Returns:
    mygroup:        hdf5 group object, new group container. If already exists returns the existent container
    myid:           str, if dealing with
    existScreen     bool, whether a Well screen already exists or not
    """
    try:
        mygroup = mycontainer.create_group(groupname)
        if 'Screen' in groupname:
            myid_file = touniqid_file(idfile, prefix=wellname, suffix=groupname.split('_')[-1], id_mode='newwell')
            mygroup.attrs['ScreenID'] = myid_file
            existScreen = False
            print('New well screen < %s > group created (incl Unique ID), retrieving empty hdf5 group' % groupname)
        else:
            print('New group < %s > created' % mygroup.name)
    except ValueError:
        mygroup = mycontainer.require_group(groupname)
        if 'Screen' in groupname:
            myid_file = touniqid_file(idfile, prefix=wellname, suffix=groupname.split('_')[-1], id_mode='existwell')
            # myid_database = mygroup.attrs['ScreenID']
            existScreen = True
            # assert myid_file == myid_database, 'Screen ID from database and ID-FILE does not match, fix before proceeding!'
            print('Well screen < %s > already exists. Retrieving existing hdf5 group' % groupname)
        else:
            print('Group < %s > already exists' % mygroup.name)

    if 'Screen' in groupname:
        return mygroup, myid_file, existScreen
    else:
        return mygroup


def checkduplicatewell(mywells, tocheck=''):
    for ii in mywells:
        if tocheck in ii:
            print('Entry already exists')


def str_infile(myfile, mystr, level='wellscreen'):
    """ Has to be restructured to look also for sample collections and measurement types....
    Looks for the line that contains the given string.
        Arguments:
        ----------
    myfile:         str, full path of the file of interest
    mystr:          str, string BEFORE the one of interest
        Returns:
        --------
    Line containing the string of interest, as a string object
    """
    match_str = []
    with open(myfile, 'r') as fp:
        # counter = 0
        for idx, line in enumerate(fp):
            match = re.search(mystr, line)
            if match:
                match_str.append(line.rstrip())
    if len(match_str) == 0:
        warnings.warn('Key for %s:%s not found' % (level, mystr))
    elif (len(match_str) > 1) and (level is 'wellscreen'):
        sys.exit('More than one key with < %s > has been found. Cannot proceed before this is fixed.' % mystr)
    else:
        return match_str


def touniqid_file(myfile, prefix='', suffix='', id_mode='existwell'):
    """" Store the generated id in a specific file. Allows creating or
    updating an existing file.
        Arguments:
    myfile:
    id_mode:       str, 'newwell', 'existwell'
    prefix:      str, e.g. locationdata
    suffix:      str, e.g. screen name
        Return:
    uniqueid:   str, new or unique identifier based on a given prefix and suffix
    """
    # If the ID file does not exist:
    if not os.path.isfile(myfile):
        print('ID file not found, creating a new one in < %s >' % os.path.split(myfile)[0])
        logfile = open(myfile, 'w')
        logfile.write('#well_id + unique_id + screen_id\n')
        logfile.close()
        id_mode = 'newwell'

    if id_mode is 'newwell':
        # Create Well ID:
        myuniqid = uniqid(prefix=prefix, suffix=suffix, more_entropy=False)
        print('Appending unique id to %s file' % os.path.split(myfile)[-1])
        with open(myfile, 'a') as logfile:
            logfile.write('%s\n' % myuniqid)
            logfile.flush()

    elif id_mode is 'existwell':
        print('Existing well. Searching for unique id of well %s, Screen %s' % (prefix, suffix))
        # with open(myfile, 'r') as logfile:
        #     filecont = logfile.readlines()
        #     no_matchids = 0
        #     for ii, cur_line in enumerate(filecont):
        #         if ('%s_' % prefix in cur_line) and ('_%s' % suffix in cur_line):
        #             no_matchids += 1
        #             if no_matchids > 1:
        #                 sys.exit('More than one id has been found for the specified well screen. Terminating process')
        #             myuniqid = np.array([str(cur_line.strip())])[0]
        #     if no_matchids == 0:
        #         sys.exit('No well screen ID found. Access denied')
        # print('Unique ID loaded successfully')
    # return myuniqid
    return 'Deprecated'


def uniqid(prefix='', suffix='', more_entropy=False):
    """ Create a unique identifier and attach a prefix and suffix to it.
        Arguments:
    prefix:      str, e.g. locationdata
    suffix:      str, e.g. screen name
    more_entropy:    bool, increase entropy for the id
        Return:
    unique identifier as a string value
        Notes:
    Modified from : http://gurukhalsa.me/2011/uniqid-in-python/
    """
    m = time.time()
    myuniqid = '%8x%05x' % (np.floor(m), (m - np.floor(m)) * 1000000)
    if more_entropy:
        valid_chars = list(set(string.hexdigits.lower()))
        entropy_string = ''
        for i in range(0, 10, 1):
            entropy_string += random.choice(valid_chars)
        myuniqid = myuniqid + entropy_string
    myuniqid = prefix + '_' + myuniqid + '_' + suffix

    return myuniqid


def addattrs(mygroup, mydata):
    """ Add attributes using as a source a Pandas DataFrame """
    if 'Lithology' in mygroup.name:
        no_args = 6
    elif 'WellDepth' in mygroup.name:
        no_args = 5
    elif 'Datum' in mygroup.name:
        no_args = 4

    if ('Lithology' in mygroup.name) or ('WellDepth' in mygroup.name) or ('Datum' in mygroup.name):
        no_entries = np.arange(1, (len(mydata) / no_args) + 1, 1)
        entries_ids = np.array([])
        for ii in no_entries:
            entries_ids = np.r_[entries_ids, np.ones((no_args,)) * ii].astype(int)
    else:
        pass

    for ii in range(0, len(mydata), 1):
        if ('Lithology' in mygroup.name) or ('WellDepth' in mygroup.name) or ('Datum' in mygroup.name):
            str_name = '%s_%s' % (entries_ids[ii], mydata.field[ii])
        else:
            str_name = mydata.field[ii]
        mygroup.attrs[str_name] = mydata.entry[ii]


def addattrs_dict(mygroup, attr_dict):
    """ Add attributes to an HDF5 group from data in a dictionary:

    mygroup:    hdf5 group object, sample group (e.g. measParamType:field, processed...) where metadata is to be
                attached
    attr_dict:  dict, keys and values of metadata to be attached to the HDF5 group
    """

    for idx, cur_key in enumerate(list(attr_dict.keys())):
        mygroup.attrs[cur_key] = list(attr_dict.values())[idx]


def errorhandle(nooptions=3):
    """ Robust error handling, only integers are accepted and
    only within the range of nooptions
        Arguments:
        ----------
    nooptions:      int, number of options on the menu. Default = 3. 0 to exit.
        Returns:
        --------
    user_input:     int, option selected by the user
    """
    is_valid = 0

    # Waits for valid input in while...not
    while not is_valid:
        try:
            user_input = int(input('Enter your choice [1-%d] : ' % nooptions))
            is_valid = 1  # set it to 1 to validate input and to terminate the while..not loop
        except ValueError as e:
            print("'%s' is not an integer." % e.args[0].split(": ")[1])

    if user_input == 0:
        sys.exit('Exiting...')

    while user_input > nooptions:

        try:
            user_input = int(input('Enter your choice [1-%d] : ' % nooptions))
            if user_input > nooptions:
                print("'%s' is not a valid integer." % user_input)

        except ValueError as e:
            print("'%s' is not an integer." % e.args[0].split(": ")[1])

    return user_input


def check_userinput(question, myoptions=''):
    """ Checks the user input before sending the input to the function call. In this way,
    the answers of the user are constrained and an error is not returned due to wrong
    user input
        Arguments:
        ----------
    question:       str, question that wants to be asked to the user
    myoptions:      list of str, available options to be consider in the answer
        Returns:
        --------
    answer:         str, with the right answer provided by the user
    """
    is_valid = 0
    while not is_valid:
        answer = input(question)
        if answer == '':
            print('Empty answer, try again!')
            pass
        elif any(answer in s for s in myoptions):
            is_valid = 1  # set it to 1 to validate input and to terminate the while..not loop
        else:
            print('%s is not a valid answer!' % answer)
    return answer


def default_answer(question, def_value, mytype):
    """ Check user input and transform it to proper type, if wrong value type is provided
    default values are taken. This is to control user's input when it is not possible
    to restrict the answers
        Arguments:
        ----------
    question:           str, question for the user
    def_value:          default value
    mytype:             type of expected answer, e.g. int, str, float
        Returns:
        --------
    tr_answer:          proper answer or given default value
    """
    try:
        myanswer = input(question)
        tr_answer = eval('mytype(myanswer)')
    except (ValueError, TypeError):
        print('Error in input value...\ntaking default: %s' % def_value)
        tr_answer = def_value

    return tr_answer


def def_dir(sender):
    """
    Evaluate whether the given filename or directory exists
    """
    mypath = r'%s' % sender.value
    if (not os.path.isdir(mypath)) and (not os.path.isfile(mypath)):
        print('Directory (or file) < %s > is NOT valid!!!' % mypath)
    elif os.path.isdir(mypath) or (os.path.isfile(mypath)):
        print('Directory (or file) < %s > is valid' % mypath)

    return mypath


def existingscreens(WellName, database):
    """ Creates a list with the available  screens of a given well
        Arguments:
    WellName:       str, selected well
    database:       str, or hdf5 object. Database of interest
        Returns:
    list, existing screen IDs for the given well
    """
    if WellName in list(database.keys()):
        screen_list = list(database['%s/Location' % WellName].keys())
        screens = []
        for aa in screen_list:
            if 'Screen' in aa:
                screens.append(aa)
    return screens


def correctFileNames(mypath):
    """ Correct the filenames. e.g. 1_2.txt --> 1-2.txt, or cmt4-4.txt --> CMT44.txt
    Args:
        mypath: str,    location of all files: e.g. r'...\pumpingtests\evolution'
    Returns:
        files renamed in the same folder
    """
    dates = os.listdir(mypath)
    # Rename files:
    for ii in dates:
        fileList = os.listdir(os.path.join(mypath, ii))
        for xx in fileList:
            old_string = xx
            if (not 'cmt' in old_string) and (not 'modif' in old_string) and (not 'log' in old_string):
                new_string = old_string.replace('_', '-')
            elif (not 'cmt' in old_string) and ('modif' in old_string) and (not 'log' in old_string):
                if len(old_string.split('_')) == 2:
                    new_string = old_string
                elif len(old_string.split('_')) > 2:
                    new_string = '%s-%s%s' % (old_string.split('_')[0], old_string.split('_')[1], old_string[3:])
            elif 'cmt' in old_string:
                new_string = old_string.replace('-', '')
                new_string = old_string.replace('cmt', 'CMT')
            if 'log' not in old_string:
                os.rename(os.path.join(mypath, ii, old_string), os.path.join(mypath, ii, new_string))
