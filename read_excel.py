import numpy as np
import pandas as pd
import h5py
import os
import load_flavors_func as ldf
import aux_functions as auxf
import math
import tables
# import platform
mylist = []


def dbstructure(name):
    print(name)
    mylist.append(name)
    #return mylist

f = h5py.File(r'C:\Users\IRTG\Dropbox\LauswiesenDatabase\Lauswiesen_db.hdf5', 'a')
#mygoup = f['/B1/Location/Screen_B1/02-12-2016/pumpingtest/field/evolution']
#ldf.loadsets(r'C:\Users\IRTG\Dropbox\Simon_database\_measurement_example\02-12-06\pumpingtest\field\evolutionfiberopt_presstrans\measurement_1\B1.txt', 'evolution', mygoup)
#mydata, metadata = load_fiberoptic(r'C:\Users\IRTG\Dropbox\Simon_database\_measurement_example\02-12-06\pumpingtest\field\evolutionfiberopt_presstrans\measurement_1', 'B1.txt',getMetadata=True )
sourcedir = r'C:\Users\IRTG\Dropbox\LauswiesenDatabase\ipynb_2_DataSetsUpload'
ldf.loadbatch('datasets', f, sourcedir, r'C:\Users\IRTG\Dropbox\Simon_database\screen.uniqueids')


# baseh5file = 'Lauswiesen_database.hdf5'
# filepath = r'C:\Users\IRTG\Dropbox\Simon_database\Data Upload'
# # filepath = r'C:\Users\IRTG\Dropbox\Simon_database\Data Upload'
# filename = 'B10_Well_v2.xlsx'
# screen_id_file = os.path.join(os.path.split(filepath)[0], 'screen.uniqueids')
# # sourcedir = r'C:\Users\IRTG\Dropbox\Simon_database\Well_Files_05.08.2016'
#
# loadbatch('datasets', f, sourcedir, screen_id_file)
#
# ldf.loadflavor('wells', 'batch', os.path.abspath(os.path.join(filepath, '..', baseh5file)), sourcedir, screen_id_file)
# #
# #
# # ldf.loadflavor('batch', os.path.abspath(os.path.join(filepath, '..',baseh5file)), filepath, screen_id_file, filetype='excel')
#
# # Create main file. 'a': Open read-write (create if doesn't exist):
# # Scan through database nodes
# f = h5py.File(os.path.join(filepath, '..', baseh5file), 'a')
# mylist = f.visit(dbstructure)
# print(mylist)
# list(f.items())
# list(f['1-1'].attrs)
#
#
#
# # Load all data from excel, this should be done just in case a new well is added:
# locationdata = pd.read_excel(os.path.join(filepath, filename), sheetname='Location',
#                              header=0, skiprows=1, parse_cols="A,B")
# datumdata = pd.read_excel(os.path.join(filepath, filename), sheetname='Datum',
#                           header=0, skiprows=1, parse_cols="A,B")
# depthdata = pd.read_excel(os.path.join(filepath, filename), sheetname='WellDepth',
#                           header=0, skiprows=1, parse_cols="A,B")
# lithologydata = pd.read_excel(os.path.join(filepath, filename), sheetname='Lithology',
#                               header=0, skiprows=1, parse_cols="A,B")
# screendata = pd.read_excel(os.path.join(filepath, filename), sheetname='WellScreen',
#                            header=0, skiprows=1, parse_cols="A,B")
# samplecollectiondata = pd.read_excel(os.path.join(filepath, filename), sheetname='SampleCollection',
#                                      header=0, skiprows=1, parse_cols="A,B")
# measurementdata = pd.read_excel(os.path.join(filepath, filename), sheetname='Measurement',
#                                 header=0, skiprows=1, parse_cols="A,B")
#
# data_dict = {'a': locationdata, 'b': datumdata, 'c': depthdata, 'd': lithologydata, 'e': screendata, 'f': samplecollectiondata, 'g': measurementdata}
#
# for cur_key, cur_value in data_dict.items():
#     cur_value.columns = ['field', 'entry']
#
# well_name = locationdata.entry[0]
# screen_name = screendata.entry[np.where(screendata.field == 'ScreenName')[0][0]]
#
# # Drop extra entries in Lithology and well depth
# depthdata = depthdata.dropna(subset=['entry']).reset_index(drop=True)
# lithologydata = lithologydata.dropna(subset=['entry']).reset_index(drop=True)
#
# # Main Container and ID for the well of interest:
# try:
#     mainContainer = f.create_group(well_name)
#     id_mode = 'newwell'
#     print('New well added to the database: %s' % locationdata.entry[0])
# except ValueError:
#     # This will read the group if already exists
#     print('Working on an existing well. Continue?')     # Todo: set up a pause so the user can decide
#     mainContainer = f.require_group(locationdata.entry[0])
#     id_mode = 'existwell'
#
# # Load or generate the screen_id:
# cur_screen_id = auxf.touniqid_file(screen_id_file, prefix=locationdata.entry[0], suffix=screen_name, id_mode=id_mode)
#
# # Create/load two subgroups for Well ID: Datum and Location
# locationGroup = auxf.addgroup(mainContainer, groupname='Location')
# datumGroup = auxf.addgroup(mainContainer, groupname='Datum')
#
# # Three subgroups of well Location: Lithology, ScreenWell and Well Depth
# depthGroup = auxf.addgroup(locationGroup, groupname='WellDepth')
# lithologyGroup = auxf.addgroup(locationGroup, groupname='Lithology')
# screenGroup = auxf.addgroup(locationGroup, groupname='Screen_%s' % screen_name)
#
# # Start dealing with Sample collections:
# # Create the sampleColl subgroup corresponding to the date
# sample_date_id = np.where(samplecollectiondata.field == 'SampleCollectionDate')[0][0]
# sample_date = pd.to_datetime(samplecollectiondata.entry[sample_date_id]).date().strftime('%d-%m-%Y')
#
# sample_type_id = np.where(samplecollectiondata.field == 'SamplingType')[0][0]
# sample_type = samplecollectiondata.entry[sample_type_id]
#
# sample_method_id = np.where(samplecollectiondata.field == 'SampleMethod')[0][0]
# sample_method = samplecollectiondata.entry[sample_method_id]
#
# # Finally, work with parameter type and define the way of loading measurements:
# measurement_param_id = np.where(measurementdata.field == 'Parameter')[0][0]
# measurement_param = measurementdata.entry[measurement_param_id]
#
# measurement_type_id = np.where(measurementdata.field == 'ParameterType')[0][0]
# measurement_type = measurementdata.entry[measurement_type_id]
#
# loadtype_id = np.where(measurementdata.field == 'LoadType')[0][0]
# loadtype = measurementdata.entry[loadtype_id]
#
# result_numeric_id = np.where(measurementdata.field == 'ResultNumeric')[0][0]
#
# bibliography_id = np.where(measurementdata.field == 'BibliographicSource')[0][0]
#
# sampleCollDate = auxf.addgroup(screenGroup, groupname=sample_date)
# sampleCollMethod = auxf.addgroup(sampleCollDate, groupname=sample_method)
# measParamType = auxf.addgroup(sampleCollMethod, groupname=measurement_type)
#
#
# # Measurements are either a result numeric or a bibliographic source:
# if not math.isnan(measurementdata.entry[result_numeric_id]):
#     resultNumeric = measurementdata.entry[result_numeric_id]
#
# if isinstance(measurementdata.entry[bibliography_id], str):
#     bibliography = measurementdata.entry[bibliography_id]
#
# samplecollectiondata = samplecollectiondata.drop([sample_method_id, sample_date_id]).reset_index(drop=True)
# measurementdata = measurementdata.drop([measurement_type_id, measurement_param_id, result_numeric_id]).reset_index(drop=True)
#
# data_dict = {'a': locationdata, 'b': datumdata, 'c': depthdata, 'd': lithologydata, 'e': screendata, 'f': samplecollectiondata, 'g': measurementdata}
# group_dict = {'a': locationGroup, 'b': datumGroup, 'c': depthGroup, 'd': lithologyGroup, 'e': screenGroup, 'f': sampleCollMethod, 'g': measParamType}
# # If new well, add metadata (from excel file) to each subgroup:
# for cur_keys in data_dict:
#     if (id_mode == 'existwell') and (cur_keys == 'a'):
#         pass
#     elif (id_mode == 'existwell') and (cur_keys == 'b'):
#         pass
#     elif (id_mode == 'existwell') and (cur_keys == 'c'):
#         pass
#     elif (id_mode == 'existwell') and (cur_keys == 'd'):
#         pass
#     elif (id_mode == 'existwell') and (cur_keys == 'e'):
#         pass
#     elif (id_mode == 'existwell') and (cur_keys == 'f'):
#         pass
#     else:
#         auxf.addattrs(group_dict[cur_keys], data_dict[cur_keys])
# f.flush()
#
# if (('resultNumeric' in locals()) is True) and (loadtype != 'external'):
#     measParamType.create_dataset(measurement_param, data=resultNumeric)
#
# if ('bibliography' in locals()) is True:
#     if (os.path.isfile(bibliography)) and (measurement_type == 'field') and (loadtype == 'external'):
#
#         dataset = auxf.loadsets(bibliography, sample_method, measParamType, full_output=False)

f.flush()
f.close()

# x = []
# for cur_group in myfile.walk_groups():
#     x.append(cur_group._v_pathname)